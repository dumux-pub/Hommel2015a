// $Id: 2pncvolumevariables.hh 5151 2011-02-01 14:22:03Z lauser $
/*****************************************************************************
 *   Copyright (C) 2008,2009 by Timo Koch,									 *
 *								Vishal Jambhekar,							 *
 * 								Alexzander Kissinger,						 *
 * 								Klaus Mosthaf,                               *
 *                              Andreas Lauser,                              *
 *                              Bernd Flemisch                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase, n-component model.
 */
#ifndef DUMUX_2PNC_VOLUME_VARIABLES_HH
#define DUMUX_2PNC_VOLUME_VARIABLES_HH

#include <iostream>
#include <vector>
#include <dune/common/parallel/collectivecommunication.hh>

#include <dumux/implicit/common/implicitmodel.hh>
#include <dumux/material/fluidstates/compositionalfluidstate.hh>
#include <dumux/common/math.hh>

#include "2pncproperties.hh"
#include "2pncindices.hh"
#include <dumux/material/constraintsolvers/computefromreferencephase2pnc.hh>
#include <dumux/material/constraintsolvers/miscible2pnccomposition.hh>

namespace Dumux
{

/*!
 * \ingroup TwoPNCModel
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase, n-component model.
 */
template <class TypeTag>
class TwoPNCVolumeVariables : public ImplicitVolumeVariables<TypeTag>
{
    typedef ImplicitVolumeVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        dim = GridView::dimension,
        dimWorld=GridView::dimensionworld,

        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        //numSPhases =  GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),
        numMajorComponents = GET_PROP_VALUE(TypeTag, NumMajorComponents),

        // formulations
        formulation = GET_PROP_VALUE(TypeTag, Formulation),
        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,

        // phase indices
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        // component indices
        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

        // phase presence enums
        nPhaseOnly = Indices::nPhaseOnly,
        wPhaseOnly = Indices::wPhaseOnly,
        bothPhases = Indices::bothPhases,

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename Grid::ctype CoordScalar;
    typedef Dumux::Miscible2pNcComposition<Scalar, FluidSystem> Miscible2pNcComposition;
    typedef Dumux::ComputeFromReferencePhase2pNc<Scalar, FluidSystem> ComputeFromReferencePhase2pNc;

public:
  
      //! The type of the object returned by the fluidState() method
      typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;
    /*!
     * \brief Update all quantities for a given control volume.
     *
     * \param primaryVariables The primary variables
     * \param problem The problem
     * \param element The element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param scvIdx The local index of the SCV (sub-control volume)
     * \param isOldSol Evaluate function with solution of current or previous time step
     */
    void update(const PrimaryVariables &primaryVariables,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {
        ParentType::update(primaryVariables,
                           problem,
                           element,
                           fvGeometry,
                           scvIdx,
                           isOldSol);
		
        completeFluidState(primaryVariables, problem, element, fvGeometry, scvIdx, fluidState_, isOldSol);
			
	    /////////////
        // calculate the remaining quantities
        /////////////
	
	const MaterialLawParams &materialParams = problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);
        
	// Second instance of a parameter cache.
        // Could be avoided if diffusion coefficients also
        // became part of the fluid state.
        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState_);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
			{// relative permeabilities
				 Scalar kr;
				 if (phaseIdx == wPhaseIdx)
					 kr = MaterialLaw::krw(materialParams, saturation(wPhaseIdx));
				 else // ATTENTION: krn requires the liquid saturation
					  // as parameter!
					 kr = MaterialLaw::krn(materialParams, saturation(wPhaseIdx));
					 mobility_[phaseIdx] = kr / fluidState_.viscosity(phaseIdx);
					 Valgrind::CheckDefined(mobility_[phaseIdx]);
				 int compIIdx = phaseIdx;
				 for(int compIdx = 0; compIdx < numComponents; ++compIdx)
				 {
				  int compJIdx = compIdx;
				  // binary diffusion coefficents
				  diffCoeff_[phaseIdx][compIdx] = 0.0;
				  if(compIIdx!= compJIdx)
				  diffCoeff_[phaseIdx][compIdx] = FluidSystem::binaryDiffusionCoefficient(fluidState_,
																					paramCache,
																					phaseIdx,
																					compIIdx,
																					compJIdx);
				  Valgrind::CheckDefined(diffCoeff_[phaseIdx][compIdx]);
				}
			}
	
    // porosity 
	porosity_ = problem.spatialParams().porosity(element,
										  	  	    	fvGeometry,
										  	  	    	scvIdx);
				
	asImp_().updateEnergy_(primaryVariables, problem,element, fvGeometry, scvIdx, isOldSol);
	
	/*totalPrecip_ = 0.0;
    for(int sPhaseIdx = 0; sPhaseIdx < numSPhases; ++sPhaseIdx)
    {
    	solidity_[sPhaseIdx] = primaryVariables[numComponents + sPhaseIdx];
    	totalPrecip_+= solidity_[sPhaseIdx];
    }
//      porosity_ = InitialPorosity_ - totalPrecip_;
    porosity_ = std::max(1e-4, std::max(0.0, InitialPorosity_ - totalPrecip_));

	// kozeny-Carman relation
	permFactor_  =  std::pow(((1-InitialPorosity_)/(1-porosity_)),2)
					* std::pow((porosity_/InitialPorosity_),3);
    */
	}

      /*!
    * \copydoc BoxModel::completeFluidState
    * \param isOldSol Specifies whether this is the previous solution or the current one
    */
  static void completeFluidState(const PrimaryVariables& primaryVariables,
				  const Problem& problem,
				  const Element& element,
				  const FVElementGeometry& fvGeometry,
				  int scvIdx,
				  FluidState& fluidState,
				  bool isOldSol = false)
    
    {
        Scalar t = Implementation::temperature_(primaryVariables, problem, element,
                                                fvGeometry, scvIdx);
        fluidState.setTemperature(t);
      
		int globalVertIdx = problem.model().dofMapper().map(element, scvIdx, dim);
        int phasePresence = problem.model().phasePresence(globalVertIdx, isOldSol);
	
	/////////////
        // set the saturations
        /////////////
	
	Scalar Sg;
        if (phasePresence == nPhaseOnly)
            Sg = 1.0;
        else if (phasePresence == wPhaseOnly) {
            Sg = 0.0;
        }
        else if (phasePresence == bothPhases) {
            if (formulation == plSg)
                Sg = primaryVariables[switchIdx];
            else if (formulation == pgSl)
                Sg = 1.0 - primaryVariables[switchIdx];
            else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");
        }   
	else DUNE_THROW(Dune::InvalidStateException, "phasePresence: " << phasePresence << " is invalid.");
	    fluidState.setSaturation(nPhaseIdx, Sg);
        fluidState.setSaturation(wPhaseIdx, 1.0 - Sg);
    
	        /////////////
        // set the pressures of the fluid phases
        /////////////

        // calculate capillary pressure
        const MaterialLawParams &materialParams
        = problem.spatialParams().materialLawParams(element, fvGeometry, scvIdx);
        Scalar pc = MaterialLaw::pc(materialParams, 1 - Sg);

        // extract the pressures
        if (formulation == plSg) {
            fluidState.setPressure(wPhaseIdx, primaryVariables[pressureIdx]);
            if (primaryVariables[pressureIdx] + pc < 0.0)
                        	DUNE_THROW(Dumux::NumericalProblem,"Capillary pressure is too low");
            fluidState.setPressure(nPhaseIdx, primaryVariables[pressureIdx] + pc);
        }
        else if (formulation == pgSl) {
            fluidState.setPressure(nPhaseIdx, primaryVariables[pressureIdx]);
// Here we check for (p_g - pc) in order to ensure that (p_l > 0)
            if (primaryVariables[pressureIdx] - pc < 0.0)
            {
            	std::cout<< "p_g: "<< primaryVariables[pressureIdx]<<" Cap_press: "<< pc << std::endl;
            	DUNE_THROW(Dumux::NumericalProblem,"Capillary pressure is too high");
            }
//        	std::cout<< "p_g: "<< primaryVariables[pressureIdx]<<" Cap_press: "<< pc << std::endl;
            fluidState.setPressure(wPhaseIdx, primaryVariables[pressureIdx] - pc);
        }
        else DUNE_THROW(Dune::InvalidStateException, "Formulation: " << formulation << " is invalid.");

	    /////////////
        // calculate the phase compositions
        /////////////

	typename FluidSystem::ParameterCache paramCache;

        // now comes the tricky part: calculate phase composition
        if (phasePresence == bothPhases) {
            // both phases are present, phase composition results from
            // the gas <-> liquid equilibrium. This is
            // the job of the "MiscibleMultiPhaseComposition"
            // constraint solver

        	// set the known mole fractions in the fluidState so that they
        	// can be used by the Miscible2pNcComposition constraint solver
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
            	fluidState.setMoleFraction(wPhaseIdx, compIdx, primaryVariables[compIdx]);
            }

            Miscible2pNcComposition::solve(fluidState,
					    paramCache,
					    wPhaseIdx,	//known phaseIdx
					    /*setViscosity=*/true,
					    /*setInternalEnergy=*/false);
        }
        else if (phasePresence == nPhaseOnly){

            Dune::FieldVector<Scalar, numComponents> moleFrac;


            moleFrac[wCompIdx] =  primaryVariables[switchIdx];

            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            	 moleFrac[compIdx] = primaryVariables[compIdx];


            Scalar sumMoleFracNotGas = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            	 sumMoleFracNotGas+=moleFrac[compIdx];

            sumMoleFracNotGas += moleFrac[wCompIdx];
            moleFrac[nCompIdx] = 1 - sumMoleFracNotGas;


            // Set fluid state mole fractions
            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            	 fluidState.setMoleFraction(nPhaseIdx, compIdx, moleFrac[compIdx]);


            // calculate the composition of the remaining phases (as
            // well as the densities of all phases). this is the job
            // of the "ComputeFromReferencePhase2pNc" constraint solver
             ComputeFromReferencePhase2pNc::solve(fluidState,
                                             paramCache,
                                             nPhaseIdx,
                                             /*setViscosity=*/true,
                                             /*setInternalEnergy=*/false);

	     }
        else if (phasePresence == wPhaseOnly){
	    // only the liquid phase is present, i.e. liquid phase
	    // composition is stored explicitly.
	    // extract _mass_ fractions in the gas phase
            Dune::FieldVector<Scalar, numComponents> moleFrac;
            
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
            	moleFrac[compIdx] = primaryVariables[compIdx];
            }
            moleFrac[nCompIdx] = primaryVariables[switchIdx];
            Scalar sumMoleFracNotWater = 0;
            for (int compIdx=numMajorComponents; compIdx<numComponents; ++compIdx)
            {
            		sumMoleFracNotWater+=moleFrac[compIdx];
            }
            sumMoleFracNotWater += moleFrac[nCompIdx];
            moleFrac[wCompIdx] = 1 -sumMoleFracNotWater;


            // convert mass to mole fractions and set the fluid state
            for (int compIdx=0; compIdx<numComponents; ++compIdx)
            {
            	fluidState.setMoleFraction(wPhaseIdx, compIdx, moleFrac[compIdx]);
            }
            
            // calculate the composition of the remaining phases (as
            // well as the densities of all phases). this is the job
            // of the "ComputeFromReferencePhase2pNc" constraint solver
            ComputeFromReferencePhase2pNc::solve(fluidState,
                                             paramCache,
                                             wPhaseIdx,
                                             /*setViscosity=*/true,
                                             /*setInternalEnergy=*/false);
        }
        paramCache.updateAll(fluidState);
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            Scalar rho = FluidSystem::density(fluidState, paramCache, phaseIdx);
            Scalar mu = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);

            fluidState.setDensity(phaseIdx, rho);
            fluidState.setViscosity(phaseIdx, mu);
        }
    }
		/*
    Scalar solidity(int phaseIdx) const
    { return solidity_[phaseIdx - numPhases]; }

    Scalar InitialPorosity() const
    { return InitialPorosity_;}

    Scalar permFactor() const
    { return permFactor_; }
*/
    /*!
     * \brief Returns the phase state for the control-volume.
     */
    const FluidState &fluidState() const
    { return fluidState_; }

    /*!
     * \brief Returns the effective saturation of a given phase within
     *        the control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar saturation(int phaseIdx) const
    { return fluidState_.saturation(phaseIdx); }

    /*!
     * \brief Returns the mass density of a given phase within the
     *        control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar density(int phaseIdx) const
    {
    	if (phaseIdx < numPhases)
    		return fluidState_.density(phaseIdx);

    	//else if (phaseIdx < numPhases+numSPhases)
    	//		return FluidSystem::saltDensity(phaseIdx);
    	else
    		DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Returns the mass density of a given phase within the
     *        control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar molarDensity(int phaseIdx) const
    {
    	if (phaseIdx < numPhases)
    		return fluidState_.molarDensity(phaseIdx);

    	//else if (phaseIdx < numPhases+numSPhases)
    	//	return FluidSystem::saltMolarDensity(phaseIdx);
    	else
        	DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Returns the effective pressure of a given phase within
     *        the control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar pressure(int phaseIdx) const
    { return fluidState_.pressure(phaseIdx); }

    /*!
     * \brief Returns temperature inside the sub-control volume.
     *
     * Note that we assume thermodynamic equilibrium, i.e. the
     * temperature of the rock matrix and of all fluid phases are
     * identical.
     */
    Scalar temperature() const
    { return fluidState_.temperature(/*phaseIdx=*/0); }

    /*!
     * \brief Returns the effective mobility of a given phase within
     *        the control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar mobility(int phaseIdx) const
    {
        return mobility_[phaseIdx];
    }

    /*!
     * \brief Returns the effective capillary pressure within the control volume.
     */
    Scalar capillaryPressure() const
    { return fluidState_.pressure(FluidSystem::nPhaseIdx) - fluidState_.pressure(FluidSystem::wPhaseIdx); }

    /*!
     * \brief Returns the average porosity within the control volume.
     */
    Scalar porosity() const
    { return porosity_; }


    /*!
     * \brief Returns the binary diffusion coefficients for a phase
     */
    Scalar diffCoeff(int phaseIdx, int compIdx) const
    { return diffCoeff_[phaseIdx][compIdx]; }

protected:
  
    static Scalar temperature_(const PrimaryVariables &priVars,
                               const Problem& problem,
                               const Element &element,
                               const FVElementGeometry &fvGeometry,
                               int scvIdx)
    {
        return problem.temperatureAtPos(fvGeometry.subContVol[scvIdx].global);
    }
    
    template<class ParameterCache>
    static Scalar enthalpy_(const FluidState& fluidState,
                            const ParameterCache& paramCache,
                            int phaseIdx)
    {
        return 0;
    }
	
	/*!
	 * \brief Update all quantities for a given control volume.
	 *
	 * \param priVars The solution primary variables
	 * \param problem The problem
	 * \param element The element
	 * \param fvGeometry Evaluate function with solution of current or previous time step
	 * \param scvIdx The local index of the SCV (sub-control volume)
	 * \param isOldSol Evaluate function with solution of current or previous time step
	 */
	void updateEnergy_(const PrimaryVariables &priVars,
					   const Problem &problem,
					   const Element &element,
					   const FVElementGeometry &fvGeometry,
					   const int scvIdx,
					   bool isOldSol)
	{ };

    Scalar porosity_;        //!< Effective porosity within the control volume
    Scalar mobility_[numPhases];  //!< Effective mobility within the control volume
    //Scalar solidity_[numSPhases];
    Scalar density_;
    //Scalar permFactor_;
    FluidState fluidState_;
    Scalar theta_;
    Scalar InitialPorosity_;
    Scalar molWtPhase_[numPhases];
    //Scalar totalPrecip_;

    Dune::FieldMatrix<Scalar, numPhases, numComponents> diffCoeff_;

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }  
    
};

} // end namespace

#endif
