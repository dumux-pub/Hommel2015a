[Newton]
NewtonTolerance = 1e-6 #

[Model]
PlausibilityTolerance = 1e-6 #
pHMin = 2 #
pHMax = 12 #
uCurExtrapolation = 0 # 1: set uCur_ to a linear extrapolation from the last two timesteps 0: use the standard uCur_ = uPrev_

[Problem]
Name = Slow_pulsed #
Inverse = 0 # 0 for normal model, 1 for use as forward model in an inverse model run

[Initial]
initDensityW = 1087 # 			[kg/m³]
initPressure = 1e5 # 			[Pa]

initxlTC = 2.3864e-7 #	 		[mol/mol]
initxlNa = 0.0 # 				[mol/mol]
initxlCl = 0.0 # 				[mol/mol]
initxlCa = 0.0 # 				[mol/mol]
initxlUrea = 0.0 # 				[mol/mol]
initxlTNH = 3.341641e-3 #	 	[mol/mol]
initxlO2 = 4.4686e-6 # 			[mol/mol]
initxlSubstrate = 2.97638e-4 # 	[mol/mol]
initxlBiosusp = 0.0 # 			[mol/mol]

initBiofilm = 0.0002597021 #	[-]
initCalcite = 0.0 # 			[-]

xlNaCorr = 2.9466e-6 # 			[mol/mol]      //NaCorr to get the pH to 6.2 calculated as molefraction
xlClCorr = 0.0 # 				[mol/mol]

#initBiofilmDistribution = 0 #			homogeneous
#initBiofilmDistribution = 1 # 			1st order in space
#initBiofilmDistribution = -1 # 		1st order in space inverted
#initBiofilmDistribution = 5 #			spike
initBiofilmDistribution = 99 #			random

[Injection]
#injVolumeflux = 1.716666666667e-7 # =10.3/60/1e6 #	//[m³/s] = [ml/min] /[s/min] /[ml/m³] //no speedup
#injVolumeflux = 8.5833333333335e-7 # =10.3/60/1e6 #	//[m³/s] = [ml/min] /[s/min] /[ml/m³] //speedup 5
injVolumeflux =  3.4333333333333e-8 # =10.3/60/1e6 #	//[m³/s] = [ml/min] /[s/min] /[ml/m³] //speedup 1/5

injTC = 5.8e-7 #				 [kg/kg]		//equilibrium with atmospheric CO2 unter atmospheric pressure
injNa = 0.0 #				 [kg/m³]		//NaCl injected
injCa = 13.530742		 #		 [kg/m³]		//computed from 49 g/l CaCl2*2H2O (molar mass = 147.68g/mol --> 0.33molCa/l, equimolar with urea (20g/l and 60g/mol)) 
injUrea = 20 #					 [kg/m³]
injTNH = 3.183840574 #//3.184#	 [kg/m³]		//computed from 10 g/l NH4Cl
injO2 = 0.008 #					 [kg/m³]
injSub = 3 #					 [kg/m³]
injBiosusp = 0.0 #				 [kg/m³]#Column9		//5.6e7 cfu/ml (40e8cfu/ml~1g/l)

injNaCorr= 0.00379 #			[kg/m³]		//NaCorr to get the pH to 6.2

numInjections = 181 #
InjectionParamFile = injections/Inj_slow_pulsed.dat #

[TimeManager]
DtInitial = 1 #0.01# [s]

TEnd = 3848519 # [s]		# Column9 - without inoculation and 1st batch, injections 5 times longerer


DtMax = 2000 # [s]

[Grid]
File = grids/VanoGrid.dgf # relative path to the grids file

[SpatialParams]
Porosity = 0.4 # [-]
CritPorosity = 0.0 # [-]     
Permeability = 2e-10 # [m^2]

[BioCoefficients]
ca1 = 8.3753e-8 #0.04434 # 		// [1/s] 		kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!
ca2 = 8.5114e-7 #9.187e-4 # 	// [1/s] 		kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!
cd1 = 2.894e-8 # 				// [1/s] 		Ebigbo et al. 2010
dc0 = 3.183e-7 # 				// [1/s] 		Taylor and Jaffe 1990
kmue = 4.1667e-5  #5.787e-5 #	// [1/s] 		Connolly et al. 2013
F = 0.5 #						// [-] 			Mateles 1971
Ke = 2e-5 #						// [kg/m³] 		Hao et al. 1983
KpHa = 6.15e-10 # 				//[mol²/kgH2O²] Kim et al. 2000
Ks = 7.99e-4 #					// [kg/m³] 		Taylor and Jaffe 1990
Yield = 0.5 #					// [-] 			Seto and Alexander 1985
rhoBiofilm = 6.9 #10 # 			//[kg/m³] 		kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!

[CalciteCoefficients]
ac = 2000 # 		// [1/dm] 		Ebigbo et al. 2012  (estimated)
kdiss1 = 8.9e-3 # 	// [kgH2O/dm²s] Chou et al. 1989
kdiss2 = 6.5e-9 #  	// [mol/dm²s] 	Chou et al. 1989
kprec = 1.5e-12 # 	// [mol/dm²s] 	Zhong and Mucci 1989
ndiss = 1.0 # 		// [-] 			Flukinger and Bernard 2009
nprec = 3.27 # 		// [-] 			Zhong and Mucci 1989
Asw0 = 500.0 # 		// [1/dm] 		Ebigbo et al. 2012  (estimated using phi_0 and A/V)
	
[UreolysisCoefficients]
kub = 3.81e-4 # 			// [kg_urease/kg_bio] 		calculated from kurease fit 2014_10_2, fittet parameter!!!!!!!!!!!!
kurease = 706.6667 #41.67 #	// [mol_urea/(kg_urease s)] Lauchnor et al. 2014
nub = 1.0 # 				// [-] 						Lauchnor et al. 2014
Keu1 = 1e-88 #7.57e-7 # 	// [mol/kgH2O] 				removed from equation  Lauchnor et al. 2014
Keu2 = 1e-88 #1.27e-8 # 	// [mol/kgH2O] 				removed from equation  Lauchnor et al. 2014
KNH4 = 10000 #0.0122 # 		// [mol/kgH2O] 				no effect observed in exp, set to high value to remove any effect on r_urea Lauchnor et al. 2014
Ku = 0.355 #0.0173 #		// [mol/kgH2O] 				Lauchnor et al. 2014