Summary
=======

Hommel2015a contains a fully working version of the DuMuX code used
for the investigation of the Influence of the initial biomass amount and distribution
and the injection strategy
on the results of MICP as published in TiPM.

This module supplies the code necessary to simulate column experiments assuming initial biomass distribution
rather than accounting for inoculation and following attachment of cells.
This can be done by including initialbiofilmcolumnproblem.hh in biomin.cc.
All necessary model files of dumux-devel are given, too.

Additionally, the input files for all injection strategies
(combinations of injection rate: fast, medium, or slow and injection scheme: pulsed or continuous),
For each injection strategy, the initial biofilm distribution can be chosen by commenting in the desired
initBiofilmDistribution = xxx value in the input file.

initBiofilmDistribution = 0 #			homogeneous
#initBiofilmDistribution = 1 # 			1st order in space
#initBiofilmDistribution = -1 # 		1st order in space inverted
#initBiofilmDistribution = 5 #			spike
#initBiofilmDistribution = 99 #			random

The reference model as published in WRR, Hommel et al 2015 for the reference setup (Experiment D2 from this publication)
can be simulated by including columnproblemgeneral.hh in biomin.cc and using the reference_with_inoc._and_attachment.input input file.


The required DUNE modules are listed at the end of this README.

The Results folder contains the data used to generate the plots (in .vtu and .mat format)

Bibtex entry:
@Article{Hommel2016tipm,
  author =    {Hommel, Johannes and Lauchnor, Ellen G. and Gerlach, Robin and Cunningham, Alfred B. and Ebigbo, Anozie and Helmig, Rainer and Class, Holger},
  title =     {Investigating the influence of the initial biomass distribution and injection strategies on biofilm-mediated calcite precipitation in porous media},
  journal =   {Transport in Porous Media},
  volume = {114},
  number = {2},
  year =      {2016},
  pages =     {557--579},
  doi =       {10.1007/s11242-015-0617-3}
}



Installation
============

The easiest way to install this module is to create a new folder and to execute the file
[installHommel2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2015a/raw/master/installHommel2015a.sh)
in this folder.

```bash
mkdir -p Hommel2015a && cd Hommel2015a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Hommel2015a/raw/master/installHommel2015a.sh
sh ./installHommel2015a.sh
```
For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Applications
============


The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

  appl/co2/biomin/,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

  appl/co2/biomin/biomin.cc,

have been extracted. You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.

Used Versions and Software
==========================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

dumux...................: version 2.7-svn (/temp/hommel/DUMUX_cmake/dumux)
dune-common.............: version 2.3.1 (/temp/hommel/DUMUX_cmake/dune-common)
dune-geometry...........: version 2.3.1 (/temp/hommel/DUMUX_cmake/dune-geometry)
dune-grid...............: version 2.3.1 (/temp/hommel/DUMUX_cmake/dune-grid)
dune-istl...............: version 2.3.1 (/temp/hommel/DUMUX_cmake/dune-istl)
dune-localfunctions.....: version 2.3.1 (/temp/hommel/DUMUX_cmake/dune-localfunctions)
ALBERTA.................: no
ALUGrid.................: version 1.52 (parallel) (/temp/hommel/DUMUX_cmake/external/ALUGrid-1.52)
AmiraMesh...............: no
BLAS....................: yes
GMP.....................: yes
Grape...................: no
METIS...................: no
METIS...................: no
MPI.....................: yes (OpenMPI)
OpenGL..................: yes (add GL_LIBS to LDADD manually, etc.)
ParMETIS................: no
SuperLU-DIST............: no
SuperLU.................: yes (version 4.3 or newer)
UG......................: yes (sequential)
UMFPACK.................: no
psurface................: no
